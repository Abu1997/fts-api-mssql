﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTask.Models
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int GenderId { get; set; }

        public int DepartmentId { get; set; }

        public DateTime DateofBirth { get; set; }

        public int MarkPercentage { get; set; }
    }
}
