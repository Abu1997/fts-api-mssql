﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTask.Models
{
    public class Gender
    {
        public int GenderId { get; set; }

        public string GenderName { get; set; }
    }
}
