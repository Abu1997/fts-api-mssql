﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTask.Services
{
    public class Assitance
    {
        public static void ExceptionLog (string Message,string stackTrace)
        {
            DateTime DateForFile = DateTime.Now;
            string dateString = DateForFile.ToString("dd-MM-yyyy");
            string dateinString = DateForFile.ToString("dd-MM-yyyy hh:mm tt");
            string Path = @"G:\FTSTasks\Backend\WebApiTask\WebApiTask\LogException\Exception" + dateString + ".txt";
            if (!System.IO.File.Exists(Path))
            {
                using(StreamWriter sw = System.IO.File.CreateText(Path))
                {
                    sw.WriteLine("Date and Time:" + dateinString);
                    sw.WriteLine("Message:" + Message);
                    sw.WriteLine("StackTrace:" + stackTrace);
                    sw.WriteLine("----------------------------------------");
                    sw.WriteLine("");
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(Path))
                {
                    sw.WriteLine("Date and Time: " + dateinString);
                    sw.WriteLine("Message: " + Message);
                    sw.WriteLine("StackTrace: " + stackTrace);
                    sw.WriteLine("-----------------------------------------------------------------------------");
                    sw.WriteLine("");
                }
            }
        }

        public static void Apilog(string Url,string Text, string Query, ICollection<string>Headers)
        {
            DateTime DateForFile = DateTime.Now;
            string dateString = DateForFile.ToString("dd-MM-yyyy");
            string dateinString = DateForFile.ToString("dd-MM-yyyy hh:mm tt");
            string Path = @"G:\FTSTasks\Backend\WebApiTask\WebApiTask\ApiLogs\api" + dateString + ".txt";
            if (!System.IO.File.Exists(Path))
            {
                using (StreamWriter sw = System.IO.File.CreateText(Path))
                {
                    sw.WriteLine("Date and Time:" + dateinString);
                    sw.WriteLine("URL: " + Url);
                    sw.WriteLine("Verb: " + Text);
                    sw.WriteLine("Query: " + Query);
                    sw.WriteLine("Headers: ");
                    foreach (var data in Headers)
                    {
                        sw.WriteLine("key = " + data);
                    }
                    sw.WriteLine("-----------------------------------------------------------------------");
                    sw.WriteLine("");
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(Path))
                {
                    sw.WriteLine("Date and Time: " + dateinString);
                    sw.WriteLine("URL: " + Url);
                    sw.WriteLine("Verb: " + Text);
                    sw.WriteLine("Query: " + Query);
                    sw.WriteLine("Headers: ");
                    foreach (var data in Headers)
                    {
                        sw.WriteLine("key = " + data);
                    }
                    sw.WriteLine("-----------------------------------------------------------------------");
                    sw.WriteLine("");
                }
            }
  
        }
    }
}
