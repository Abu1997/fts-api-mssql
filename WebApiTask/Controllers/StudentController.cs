﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebApiTask.Models;
using WebApiTask.Services;

namespace WebApiTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public StudentController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("List")]
        public IActionResult GetList()
        {
            try
            {
                string query = @"select Id,Name,GenderId,DepartmentId,format(DateofBirth,'dd/MM/yyyy'),MarkPercentage 
                             from Student";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult(table);
            }
            catch(Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }

        }

        [HttpGet("single/{Id}")]
        public IActionResult GetSingle( int Id)
        {
            try
            {
                string query = @"select Id,Name,GenderId,DepartmentId,DateofBirth,MarkPercentage
                             from Student where Id = '" + Id + @"'
                               ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);
                return new JsonResult(table);
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }

        }

        [HttpGet("Joins/list")]
        public IActionResult JoinTable()
        {
            string query = @"select Id,Name,DateofBirth,MarkPercentage,DepartmentName,GenderName 
                             from Student inner join Department on Student.DepartmentId = Department.DepartmentId 
                             inner join Gender on Student.GenderId = Gender.GenderId;
                               ";
            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("DefaultString");
            SqlDataReader myReader;
            using (SqlConnection Connet = new SqlConnection(SqlDataSource))
            {
                Connet.Open();
                using (SqlCommand command = new SqlCommand(query, Connet))
                {
                    myReader = command.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    Connet.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("between-percentage/all/:from/:to")]
        public IActionResult GetPercent(int MarkPercentage)
        {
            string query = @"select Id,Name,DateofBirth,MarkPercentage,DepartmentName,GenderName 
                             from Student inner join Department on Student.DepartmentId = Department.DepartmentId 
                             inner join Gender on Student.GenderId = Gender.GenderId Where MarkPercentage >= '" + MarkPercentage + @"'
                               ";
            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("DefaultString");
            SqlDataReader myReader;
            using (SqlConnection Connet = new SqlConnection(SqlDataSource))
            {
                Connet.Open();
                using (SqlCommand command = new SqlCommand(query, Connet))
                {
                    myReader = command.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    Connet.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("api/student/by-gender/:genderId")]
        public IActionResult GetGender(int Gender)
        {
            string query = @"select Id,Name,DateofBirth,MarkPercentage,GenderName,DepartmentName from Student s,Gender g,Department d where s.GenderId = g.GenderId and 
                             s.DepartmentId = d.DepartmentId and g.GenderId = '" + Gender + @"'
                               ";
            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("DefaultString");
            SqlDataReader myReader;
            using (SqlConnection Connet = new SqlConnection(SqlDataSource))
            {
                Connet.Open();
                using (SqlCommand command = new SqlCommand(query, Connet))
                {
                    myReader = command.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    Connet.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("api/student/by-department/:departmentId")]
        public IActionResult GetDepart(int Depart)
        {
            string query = @"select Id,Name,DateofBirth,MarkPercentage,GenderName,DepartmentName from Student s,Gender g,Department d where s.GenderId = g.GenderId and 
                             s.DepartmentId = d.DepartmentId and d.DepartmentId = '" + Depart + @"'
                               ";
            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("DefaultString");
            SqlDataReader myReader;
            using (SqlConnection Connet = new SqlConnection(SqlDataSource))
            {
                Connet.Open();
                using (SqlCommand command = new SqlCommand(query, Connet))
                {
                    myReader = command.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    Connet.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("api/student/count/")]
        public IActionResult GetCount()
        {
            string query = @"select COUNT(*) from Student
                               ";
            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("DefaultString");
            SqlDataReader myReader;
            using (SqlConnection Connet = new SqlConnection(SqlDataSource))
            {
                Connet.Open();
                using (SqlCommand command = new SqlCommand(query, Connet))
                {
                    myReader = command.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    Connet.Close();
                }
            }
            return new JsonResult(table);
        }

        [HttpGet("api/list/pagination/{pageNumber}/{pageCount}")]
        public IActionResult Pagination(int pageNumber,int pageCount)
        {

            //var start = pageNumber * pageSize;
            //var end = (pageNumber * pageSize);
            

            string query = @"DECLARE @PageNumber AS INT
                             DECLARE @RowsOfPage AS INT
                             SET @PageNumber='" + pageNumber + @"'
                             SET @RowsOfPage='" + pageCount + @"'
                             SELECT Id,Name,MarkPercentage FROM Student
                             ORDER BY Id  
                             OFFSET (@PageNumber-1)*@RowsOfPage ROWS
                             FETCH NEXT @RowsOfPage ROWS ONLY
                               ";
            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("DefaultString");
            SqlDataReader myReader;
            using (SqlConnection Connet = new SqlConnection(SqlDataSource))
            {
                Connet.Open();
                using (SqlCommand command = new SqlCommand(query, Connet))
                {
                    myReader = command.ExecuteReader();
                    table.Load(myReader);

                    myReader.Close();
                    Connet.Close();
                }
            }
            return new JsonResult(table);
        }


        [HttpPost("create")]
        public IActionResult CreateDepart([FromBody] Student Stu)
        {
            try
            {
                string query = @"
                             insert into Student values 
                             (
                              '" + Stu.Name + @"'
                              ,'" + Stu.GenderId + @"'
                              ,'" + Stu.DepartmentId + @"'
                              ,'" + Stu.DateofBirth + @"'
                              ,'" + Stu.MarkPercentage + @"'
                              )
                              ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult("Added Successfully");
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }


        }

        [HttpPut("Update")]
        public IActionResult UpdateDepart([FromBody] Student Stu)
        {

            try
            {
                string query = @"
                             update Student set 
                             Name='" + Stu.Name + @"'
                             ,GenderId='" + Stu.GenderId + @"'
                             ,DepartmentId='" + Stu.DepartmentId + @"'
                             ,DateofBirth='" + Stu.DateofBirth + @"'
                             ,MarkPercentage='" + Stu.MarkPercentage + @"'
                             where Id=" + Stu.Id + @"
                              ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult("Update Successfully");
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }


        }

        [HttpDelete("Delete/id")]

        public IActionResult DeleteId([FromBody] int Id)
        {

            try
            {
                string query = @"
                            delete from Student
                            where Id = " + Id + @"
                              ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult("Delete Successfully");
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }


        }
    }
}
