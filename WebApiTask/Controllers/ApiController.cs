﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApiController : ControllerBase
    {
        [HttpGet("exception-log/{date}")]
        public ActionResult GetExceptionLog(string date)
        {
            string dateinstring = date.ToString();
            string path = @"G:\FTSTasks\Backend\WebApiTask\WebApiTask\LogException\Exception" + date + ".txt";
            string[] readText = System.IO.File.ReadAllLines(path);
            foreach (string s in readText)
            {
                Console.WriteLine(s);
            }
            return Ok(string.Join("\n", readText));
        }
        [HttpGet("api-log/{date}")]
        public ActionResult GetApiLog(string date)
        {
            string dateinstring = date.ToString();
            string path = @"G:\FTSTasks\Backend\WebApiTask\WebApiTask\ApiLogs\api" + dateinstring + ".txt";
            string[] readText = System.IO.File.ReadAllLines(path);
            foreach (string s in readText)
            {
                Console.WriteLine(s);
            }
            return Ok(string.Join("\n", readText));
        }
    }
}
