﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebApiTask.Models;
using WebApiTask.Services;

namespace WebApiTask.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenderController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public GenderController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet("List")]
        public IActionResult GetList()
        {

            try
            {

                string query = @"select GenderId,GenderName
                             from Gender";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult(table);
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }

        }

        [HttpGet("single/{Id}")]
        public IActionResult GetSingle( int Id)
        {
            try
            {
                string query = @"select *
                             from Gender where GenderId = '" + Id + @"'
                               ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult(table);
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }

        }


        [HttpPost("create")]
        public IActionResult CreateDepart([FromBody] Gender gen)
        {

            try
            {
                string query = @"
                             insert into Gender(GenderName) values 
                             ('" + gen.GenderName + @"')
                              ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult("Added Successfully");
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }


        }

        [HttpPut("Update")]
        public IActionResult UpdateDepart([FromBody] Gender gen)
        {

            try
            {
                string query = @"
                             update Gender set 
                             GenderName='" + gen.GenderName + @"'
                             where GenderId=" + gen.GenderId + @"
                              ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult("Update Successfully");
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }


        }

        [HttpDelete("Delete/id")]

        public IActionResult DeleteId([FromBody] int Id)
        {

            try
            {
                string query = @"
                            delete from Gender
                            where GenderId = " + Id + @"
                              ";
                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("DefaultString");
                SqlDataReader myReader;
                using (SqlConnection Connet = new SqlConnection(SqlDataSource))
                {
                    Connet.Open();
                    using (SqlCommand command = new SqlCommand(query, Connet))
                    {
                        myReader = command.ExecuteReader();
                        table.Load(myReader);

                        myReader.Close();
                        Connet.Close();
                    }
                }

                string Url = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Text = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                Assitance.Apilog(Url, Text, Query, Headers);

                return new JsonResult("Delete Successfully");
            }
            catch (Exception e)
            {
                string stacktrace = e.StackTrace.ToString();
                string message = e.Message.ToString();
                Assitance.ExceptionLog(message, stacktrace);
                return StatusCode(500, e);
            }


        }
    }
}
